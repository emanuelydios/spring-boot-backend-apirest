package com.bolsadeideas.springboot.backend.apirest.models.services;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Role;
import com.bolsadeideas.springboot.backend.apirest.models.entity.Usuario;

import java.util.List;

public interface IUsuarioService {

	public Usuario findByUsername(String username);

	public List<Usuario> findAll();

	public Usuario saveUsuario(Usuario usuario);

	public void deleteUsuario(String username);

	public List<Role> getRoles();

}
