package com.bolsadeideas.springboot.backend.apirest.models.dao;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Distribuidor;
import org.springframework.data.repository.CrudRepository;

public interface IDistribuidorDao extends CrudRepository<Distribuidor, Long> {
}
