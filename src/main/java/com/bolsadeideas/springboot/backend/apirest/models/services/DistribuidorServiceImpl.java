package com.bolsadeideas.springboot.backend.apirest.models.services;

import com.bolsadeideas.springboot.backend.apirest.models.dao.IDistribuidorDao;
import com.bolsadeideas.springboot.backend.apirest.models.entity.Distribuidor;
import com.bolsadeideas.springboot.backend.apirest.models.entity.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DistribuidorServiceImpl implements IDistribuidorService{

    @Autowired
    private IDistribuidorDao distribuidorDao;

    @Override
    @Transactional
    public List<Distribuidor> findAll() {
        return (List<Distribuidor>)distribuidorDao.findAll();
    }

    @Override
    @Transactional
    public Distribuidor save(Distribuidor distribuidor) {
        return distribuidorDao.save(distribuidor);
    }

    @Override
    @Transactional
    public Distribuidor findById(Long id) {
        return distribuidorDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        distribuidorDao.deleteById(id);
    }
}
