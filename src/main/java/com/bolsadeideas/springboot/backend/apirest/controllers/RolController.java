package com.bolsadeideas.springboot.backend.apirest.controllers;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Role;
import com.bolsadeideas.springboot.backend.apirest.models.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api")
public class RolController {
    @Autowired
    private UsuarioService usuarioService;

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/rol")
    @ResponseStatus(HttpStatus.OK)
    public List<Role> findAll() {
        return usuarioService.getRoles();
    }
}
