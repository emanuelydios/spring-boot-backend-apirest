package com.bolsadeideas.springboot.backend.apirest.models.services;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Distribuidor;
import com.bolsadeideas.springboot.backend.apirest.models.entity.Producto;

import java.util.List;

public interface IDistribuidorService {

    public List<Distribuidor> findAll();

    public Distribuidor save(Distribuidor distribuidor);

    public Distribuidor findById(Long id);

    public void delete(Long id);
}
