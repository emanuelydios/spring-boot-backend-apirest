package com.bolsadeideas.springboot.backend.apirest.models.services;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Producto;

public interface IProductoService {

    public Producto save(Producto producto);

    public Producto findById(Long id);

    public void delete(Long id);

}
