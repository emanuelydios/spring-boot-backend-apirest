/* Populate tabla clientes */

INSERT INTO regiones (id, nombre) VALUES (1, 'Sudamerica');
INSERT INTO regiones (id, nombre) VALUES (2, 'Centroamerica');
INSERT INTO regiones (id, nombre) VALUES (3, 'Norteamerica');
INSERT INTO regiones (id, nombre) VALUES (4, 'Europa');
INSERT INTO regiones (id, nombre) VALUES (5, 'Asia');
INSERT INTO regiones (id, nombre) VALUES (6, 'Africa');
INSERT INTO regiones (id, nombre) VALUES (7, 'Oceania');
INSERT INTO regiones (id, nombre) VALUES (8, 'Antartida');

INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(1, 'Andres', 'Guzman', 'profesor@bolsadeideas.com', '2018-01-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(2, 'Mr. John', 'Doe', 'john.doe@gmail.com', '2018-01-02');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Linus', 'Torvalds', 'linus.torvalds@gmail.com', '2018-01-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Rasmus', 'Lerdorf', 'rasmus.lerdorf@gmail.com', '2018-01-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Erich', 'Gamma', 'erich.gamma@gmail.com', '2018-02-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Richard', 'Helm', 'richard.helm@gmail.com', '2018-02-10');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Ralph', 'Johnson', 'ralph.johnson@gmail.com', '2018-02-18');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'John', 'Vlissides', 'john.vlissides@gmail.com', '2018-02-28');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Dr. James', 'Gosling', 'james.gosling@gmail.com', '2018-03-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(5, 'Magma', 'Lee', 'magma.lee@gmail.com', '2018-03-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(6, 'Tornado', 'Roe', 'tornado.roe@gmail.com', '2018-03-05');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(7, 'Jade', 'Doe', 'jane.doe@gmail.com', '2018-03-06');

/* Creamos algunos usuarios con sus roles */
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq',1, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);

/*CREAMOS ALGUNOS DISTRIBUIDORES*/
INSERT INTO distribuidor (nombre, ruc, rubro, area_distribucion, direccion, nombre_contacto, apellido_contacto, telefono, correo) VALUES ( 'CocaCola', '10164090', 'Bebidas', 'Norte', 'Emancipacion 458', 'Julio', 'Vega', '987654321', 'julio_vega@gmail.com');
INSERT INTO distribuidor (nombre, ruc, rubro, area_distribucion, direccion, nombre_contacto, apellido_contacto, telefono, correo) VALUES ( 'Bakus', '10165010', 'Bebidas', 'Sur', 'Emancipacion 500', 'Sofia', 'Vergara', '995654321', 'sofia_vergara@gmail.com');


/* Populate tabla productos */
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Pantalla LCD', 15,1599.00, 1,'Panasonic','Esta es una Pantalla full HD 4K');
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Camara digital DSC-W320B', 15,750.00, 1,'Sony','Esta es una Camara');
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' iPod shuffle', 15,3500.00, 1,'Apple','Esta es una IPOD');
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Notebook Z110', 15,1500.00, 1,'Sony','Esta es una notebook');
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Packard Multifuncional F2280', 15,600.00, 1,'Hewlett','Esta es packard' );
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Bicicleta Aro 26', 15,450.00, 1,'Bianchi','Esta es una bicicleta');
INSERT INTO productos (nombre, stock,precio, distribuidor_id,marca, descripcion) VALUES(' Comoda 5 Cajones', 15,500.00, 1,'Mica','Esta es una comoda');


/* Creamos algunas facturas */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());

INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 6);